import React from "react";

//without JSX
const NavBar = () => {
    return React.createElement("div", {id: "test", className : 'dummy'}, React.createElement("h1", null, "This is NavBar"))
};

export default NavBar
