import React, { Component } from "react";
import Header from './Header'
import Footer from "./Footer";
import NavBar from "./Navbar";
import Greet from "./Greet";

//ES6
const Dashboard = () =>
    <div>
        <Header />
        <h1>Hello World</h1>
        <Greet name="Test">
            <p>Test the Paragraph</p>
        </Greet>
        <NavBar />
        <Footer />
    </div>
//
// class Dashboard extends Component{
//     render() {
//         return  <div>
//             <h1>Hello World</h1>
//             <Header />
//         </div>
//     }
// }

export default Dashboard
