import React from 'react';
import { Switch, Route, Redirect } from 'react-router';
import LoginPage from '../../pages/login/login';
import Dashboard from '../../dashboard/Dashboard';

class AppRouter extends React.Component {
    render = () => (
        <Switch>
            <Route path="/login" component={LoginPage} />
            <Route path="/dashboard" component={Dashboard} />
            <Redirect from="/" to="/login" />
        </Switch>
    );
}
export default AppRouter;
