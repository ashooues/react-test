import Button from '@material-ui/core/Button';
import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    button: {
        margin: theme.spacing(1),
    },
    leftIcon: {
        marginRight: theme.spacing(1),
    },
    rightIcon: {
        marginLeft: theme.spacing(1),
    },
    iconSmall: {
        fontSize: 20,
    },
    label: {
        textTransform: 'none',
        color: 'black',
    },
}));
class ButtonComponent extends React.Component {
    handleShowLabel() {
        this.setState((prevState) => ({
            label: !prevState.label,
            boxColor: !prevState.boxColor,
        }), () => { });
    }

    render() {
        const { label, classes, boxColor } = this.props;
        return (
            <div>
                <Button classes={{}} variant="contained" color={boxColor} className={`${classes.button}, ${boxColor}`}>
                    {label}
                </Button>
            </div>
        );
    }
}
export default withStyles(useStyles)(ButtonComponent);
