import { TextField } from '@material-ui/core';
import React from 'react';
import Visibility from '@material-ui/icons/Visibility';
import withStyles from '@material-ui/core/styles/withStyles';
import './inputBox.scss';


const styles = {
    icon: {
        color: '#959CAE',
        marginLeft: '-1.5rem',
        position: 'absolute',
        marginTop: '6px',
    },
};
class TextInput extends React.Component {
    state = {
        showPassword: false,
    };

    componentDidMount() {

    }

    render = () => {
        const { showPasswordOption, classes } = this.props;
        const { showPassword } = this.state;
        return (
            <>
                {showPasswordOption
                    ? (
                        <>
                            <TextField className="passwordInput" onChange={(event) => { this.handleInputChange(event, 'text'); }} {...this.props} type={showPassword ? 'text' : 'password'} />
                            <>
                                {showPassword ? (
                                        <Visibility
                                            className={classes.icon}
                                            onClick={() => {
                                                this.handleShowPassword();
                                            }}
                                        />
                                    )
                                    : <Visibility className={classes.icon} style={{ color: '#ebebeb' }} onClick={() => { this.handleShowPassword(); }} />}
                            </>
                        </>
                    )
                    : <TextField onChange={(event) => { this.handleInputChange(event, 'text'); }} {...this.props} />}
            </>
        );
    };

    handleShowPassword() {
        this.setState((prevState) => ({
            showPassword: !prevState.showPassword,
        }), () => { });
    }

    handleInputChange(event) {
        const { onInputChange, validationRegex, getValidation } = this.props;
        if (!validationRegex || validationRegex.test(String(event.target.value).toLowerCase())) {
            getValidation(true);
        } else {
            getValidation(false);
        }
        onInputChange(event);
    }
}
export default withStyles(styles)(TextInput);
