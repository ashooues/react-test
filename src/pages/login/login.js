import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';
import ButtonComponent from '../../components/buttons/button';
import TextInput from '../../components/inputs/inputBox';
import './login.scss';

const useStyles = {
    card: {
        textAlign: 'center',
        height: '420px',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    img_style: {
        paddingTop: '56.25%',
        height: '50px',
        width: 'auto',
        padding: '25px 0',
    },
};
class LoginPage extends React.Component {
    render = () => {
        const classes = this.props;
        const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return (
            <Card className="card">
                <CardMedia
                    component="img"
                    alt="Beehyv Logo"
                    height="50"
                    className={classes.img_style}
                    image="/assets/beehyv.png"
                    title="Beehyv Logo"
                />
                <CardContent>
                    <Typography className={classes.title} color="textPrimary" gutterBottom>
                        Welcome back!
                    </Typography>
                    <Typography>
                        <TextInput required validationRegex={emailRegex} getValidation={() => {}} placeholder="Username" onInputChange={() => { }} />
                    </Typography>
                    <Typography>
                        <TextInput
                            type="password"
                            required
                            showPasswordOption={true}
                            getValidation={() => { }}
                            placeholder="Enter Password"
                            onInputChange={() => { }}
                        />
                    </Typography>
                </CardContent>
                <ButtonComponent size="large" label="Sign In" boxColor="button_modified" />
                <ButtonComponent size="large" label="Cancel" boxColor="button_modified" />
                <Typography color="textSecondary" gutterBottom>
                    <span className="forget_password_text"><u>Forgot your password?</u></span>
                </Typography>
            </Card>
        );
    }
}

export default withStyles(useStyles)(LoginPage);
